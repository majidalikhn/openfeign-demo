package com.demo.feign.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Component
public class AddressPojo {

    private String id;

    private double latitude;

     private double longitude;

    private int zipCode;

    private String location;

}
