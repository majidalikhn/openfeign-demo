package com.demo.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "addressClient", url = "${client.url}")
public interface AddressClient {

    @RequestMapping(method = RequestMethod.GET)
    String readData();

}
