package com.demo.feign.controller;

import com.demo.feign.client.AddressClient;
import com.demo.feign.client.AddressPojo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/address")
public class AddressController {

    private final AddressClient addressClient;

    public AddressController(AddressClient addressClient) {
        this.addressClient = addressClient;
    }

    @GetMapping("")
    public List<AddressPojo> getChargingStations() throws JsonProcessingException {
        var mapper = new ObjectMapper();
        var mapCollectionType = mapper.getTypeFactory()
                .constructCollectionType(List.class, AddressPojo.class);
        return mapper.readValue(addressClient.readData(), mapCollectionType);
    }



}
